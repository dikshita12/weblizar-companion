<?php 

defined( 'ABSPATH' ) or die();

class green_lantern_Customizer_scripts {

	// Enqueue scripts/styles.
	public static function wl_customizer_enqueue() {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'customizer-dynamic-js', WL_COMPANION_PLUGIN_URL . 'admin/inc/controllers/green-lantern/js/dynamic_fields_green-lantern.js',array( 'jquery'), rand(), true ); 
		wp_enqueue_script( 'bootstrap-js', WL_COMPANION_PLUGIN_URL . 'admin/js/bootstrap.js' ); 
		wp_enqueue_script( 'customizer-dynamic-js', WL_COMPANION_PLUGIN_URL . 'admin/js/dynamic_fields.js', array( 'jquery'), rand(), true ); 
		wp_enqueue_script( 'customizer-iconpicker', WL_COMPANION_PLUGIN_URL . 'admin/js/fontawesome-iconpicker.js', array( 'jquery'), rand(), true );
		wp_enqueue_media();

		wp_enqueue_style( 'Bootstrap-css', WL_COMPANION_PLUGIN_URL . 'admin/css/bootstrap.min.css', array(), rand() );
		wp_enqueue_style( 'green-lantern-font-awesome', 'https://use.fontawesome.com/releases/v5.5.0/css/all.css' );
		wp_enqueue_style( 'customizer-dynamic-css', WL_COMPANION_PLUGIN_URL . 'admin/css/dynamic_fields.css', array(), rand() );
		wp_enqueue_style( 'iconpicker-css', WL_COMPANION_PLUGIN_URL . 'admin/css/fontawesome-iconpicker.css', array(), rand() );
	} 
}
?>