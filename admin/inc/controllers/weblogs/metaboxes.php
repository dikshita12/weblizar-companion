<?php
/**
 * Add Metaboxes
 *
 * @package weblizae companion 
 */

/**
 * Add meta boxes to post
 *
 * @return void
 */
function wl_companion_add_meta_boxes() {
	add_meta_box( 'weblogs_post_metabox', esc_html__( 'Post Settings', WL_COMPANION_DOMAIN ), 'wl_companion_post_settings_html', 'post', 'side', 'default' );
}
add_action( 'add_meta_boxes', 'wl_companion_add_meta_boxes' );

/**
 * Post sidebar metabox html
 *
 * @param  WP_Post $post The post object.
 * @return void
 */
function wl_companion_post_settings_html( $post ) {
	$sidebar = get_post_meta( $post->ID, '_weblogs_post_sidebar', true );
	wp_nonce_field( 'weblogs_update_post_metabox', 'weblogs_update_post_nonce' );
	?>
	<p>
		<label for="weblogs_post_sidebar"><?php esc_html_e( 'Sidebar', WL_COMPANION_DOMAIN ); ?></label>
		<br />
		<select class="widefat" name="weblogs_post_sidebar" id="weblogs_post_sidebar">
			<option <?php selected( 'no-sidebar', $sidebar ); ?> value="no-sidebar"><?php esc_html_e( 'No Sidebar', WL_COMPANION_DOMAIN ); ?></option>
			<option <?php selected( 'right-sidebar', $sidebar ); ?> value="right-sidebar"><?php esc_html_e( 'Right Sidebar', WL_COMPANION_DOMAIN ); ?></option>
			<option <?php selected( 'left-sidebar', $sidebar ); ?> value="left-sidebar"><?php esc_html_e( 'Left Sidebar', WL_COMPANION_DOMAIN ); ?></option>
		</select>
	</p>
	<?php
}

/**
 * Save post metabox
 *
 * @param  int     $post_id The post ID.
 * @param  WP_Post $post The post object.
 * @return void
 */
function wl_companion_save_post_metabox( $post_id, $post ) {
	$edit_cap = get_post_type_object( $post->post_type )->cap->edit_post;
	if ( ! current_user_can( $edit_cap, $post_id ) ) {
		return;
	}

	if ( ! isset( $_POST['weblogs_update_post_nonce'] ) || ! wp_verify_nonce( sanitize_key( $_POST['weblogs_update_post_nonce'] ), 'weblogs_update_post_metabox' ) ) {
		return;
	}

	$post_sidebar = isset( $_POST['weblogs_post_sidebar'] ) ? sanitize_text_field( wp_unslash( $_POST['weblogs_post_sidebar'] ) ) : '';

	$post_sidebar = wl_companion_post_sidebar_sanitize( $post_sidebar );

	update_post_meta( $post_id, '_weblogs_post_sidebar', $post_sidebar );
}
add_action( 'save_post', 'wl_companion_save_post_metabox', 10, 2 );

/**
 * Sanitize post sidebar value
 *
 * @param string $input The input value.
 * @return string
 */
function wl_companion_post_sidebar_sanitize( $input ) {
	$valid = array( 'no-sidebar', 'right-sidebar', 'left-sidebar' );
	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}
	return 'no-sidebar';
}
