<?php 

defined( 'ABSPATH' ) or die();

if ( class_exists( 'WP_Customize_Control' ) ) :
class travelogged_Customizer_destination_fields_new extends WP_Customize_Control {

	public function render_content() {
		?>
		<div class="wl_agm destination_block">
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php endif; ?>
			</label>
			<form id="wl-ext-form-destination" method="post">
				<div id="input_fields_wrap-destination">
					<?php 
							if ( ! empty ( get_theme_mod( 'travelogged_destination_data') ) )  {
								$name_arr = unserialize(get_theme_mod( 'travelogged_destination_data'));
								foreach ( $name_arr as $key => $value ) {
								?>
									<div class="wl-dynamic-fields">
										<div class="form-group">
											<label for="title" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Destiantion Name', WL_COMPANION_DOMAIN ); ?></label>
											<input type="text" class="form-control" id="desti_name-<?php echo $key; ?>" name="desti_name-<?php echo $key; ?>" placeholder="<?php esc_html_e ( 'Enter Name', WL_COMPANION_DOMAIN ); ?>" value="<?php if ( ! empty ( $value['desti_name'] ) ) { echo $value['desti_name']; } ?>" >
										</div>
										<div class="form-group">
											<label for="desti_desc-<?php echo $key; ?>" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Destination Description', WL_COMPANION_DOMAIN ); ?></label>
											<textarea class="form-control" rows="5" id="desti_desc-<?php echo $key; ?>" name="desti_desc-<?php echo $key; ?>" placeholder="Description"><?php if ( ! empty ( $value['desti_desc'] ) ) { echo $value['desti_desc']; } ?></textarea>
										</div>
										<div class="form-group">
											<?php if ( ! empty ( $value['desti_image'] ) ) { ?>
												<img class="wl-upload-img-tag" src="<?php echo $value['desti_image']; ?>"><br>
											<?php }?>
											<label for="desti_image-<?php echo $key; ?>" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Destination Picture', WL_COMPANION_DOMAIN ); ?></label>
											<input type="text" name="desti_image-<?php echo $key; ?>" id="desti_image-<?php echo $key; ?>" class="form-control desti_image" value="<?php if ( ! empty ( $value['desti_image'] ) ) { echo $value['desti_image']; } ?>" >
											<input type="button" name="upload-btn" class="button-secondary button upload_image_btn upload_desti_c" id="upload_desti-<?php echo $key; ?>" value="Upload">
										</div>
										<div class="form-group">
											<label for="ratings" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Ratings', WL_COMPANION_DOMAIN ); ?></label>
											<select class="form-control" id="ratings-<?php echo $key; ?>" name="ratings-<?php echo $key; ?>" required>
												<option value="1" <?php selected( $value['ratings'], '1', 'selected' ); ?>>1</option>
												<option value="2" <?php selected( $value['ratings'], '2', 'selected' ); ?>>2</option>
												<option value="3" <?php selected( $value['ratings'], '3', 'selected' ); ?>>3</option>
												<option value="4" <?php selected( $value['ratings'], '4', 'selected' ); ?>>4</option>
												<option value="5" <?php selected( $value['ratings'], '5', 'selected' ); ?>>5</option>
											</select>
										</div>
										<div class="form-group">
											<label for="desti_duration" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Package Duration', WL_COMPANION_DOMAIN ); ?></label>
											<input type="text" class="form-control" id="desti_duration-<?php echo $key; ?>" name="desti_duration-<?php echo $key; ?>" placeholder="<?php esc_html_e ( '4 Days-5 Nights', WL_COMPANION_DOMAIN ); ?>" value="<?php if ( ! empty ( $value['desti_duration'] ) ) { echo $value['desti_duration']; } ?>" >
										</div>
										<div class="form-group">
											<label for="btn_text" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Button text', WL_COMPANION_DOMAIN ); ?></label>
											<input type="text" class="form-control" id="btn_text-<?php echo $key; ?>" name="btn_text-<?php echo $key; ?>" placeholder="<?php esc_html_e ( 'https://example.com', WL_COMPANION_DOMAIN ); ?>" value="<?php if ( ! empty ( $value['btn_text'] ) ) { echo $value['btn_text']; } ?>" >
										</div>
										<div class="form-group">
											<label for="btn_link" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Button Link', WL_COMPANION_DOMAIN ); ?></label>
											<input type="text" class="form-control" id="btn_link-<?php echo $key; ?>" name="btn_link-<?php echo $key; ?>" placeholder="<?php esc_html_e ( 'https://example.com', WL_COMPANION_DOMAIN ); ?>" value="<?php if ( ! empty ( $value['btn_link'] ) ) { echo $value['btn_link']; } ?>" >
										</div>
										<a href="#" class="btn btn-danger btn-sm remove_field"><?php esc_html_e ( 'Remove', WL_COMPANION_DOMAIN ); ?></a>
									</div>
								<?php
								}
							}
					?>
				</div>
				<button class='btn btn-success add_field_button' id="add_field_button-destination"><?php esc_html_e ( 'Add Destination', WL_COMPANION_DOMAIN ); ?></button></br>
				<button type="button" class="btn btn-success add_field_button wl-companion-submit-btn" id="wl-ext-submit-destination"><?php esc_html_e ( 'Save', WL_COMPANION_DOMAIN ); ?></button>
			</form>
		</div>
		<?php
	}

}
endif;

?>