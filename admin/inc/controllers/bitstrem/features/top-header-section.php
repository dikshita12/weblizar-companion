<?php

defined( 'ABSPATH' ) or die();

/**
 *  General options
 */
class wl_topheader_customizer {
	
	public static function wl_bitstrem_topheader_customizer( $wp_customize ) {

		/* top header Option */
		$wp_customize->add_section(
			'topheader_section',
			array(
				'title'      => __("Top Header Sections",WL_COMPANION_DOMAIN),
				'panel'      => 'bitstrem_theme_option',
				'capability' => 'edit_theme_options',
			    'priority'   => 36
			)
		);

		$wp_customize->add_setting(
			'topheader_home',
			array(
				'type'              => 'theme_mod',
				'default'           => 1,
				'sanitize_callback' => 'bitstrem_sanitize_checkbox',
				'capability'        => 'edit_theme_options'
			)
		);
		$wp_customize->add_control( 
			'bitstrem_show_topheader', 
			array(
				'label'    => __( 'Enable Contact Bar', WL_COMPANION_DOMAIN ),
				'type'     =>'checkbox',
				'section'  => 'topheader_section',
				'settings' => 'topheader_home'
			) 
		);

		$wp_customize->add_setting(
			'bitstrem_topheader_timing',
			array(
				'default'           => '',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'bitstrem_sanitize_text',
				'capability'        => 'edit_theme_options'
			)
		);

		$wp_customize->add_control( 
			'bitstrem_topheader_timing', 
			array(
				'label'    =>  __( 'Timing', WL_COMPANION_DOMAIN ),
				'type'     => 'text',
				'section'  => 'topheader_section',
				'settings' => 'bitstrem_topheader_timing'
			) 
		);

		$wp_customize->add_setting(
			'bitstrem_topheader_ctitle',
			array(
				'default'           => 'Call Us',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'bitstrem_sanitize_text',
				'capability'        => 'edit_theme_options'
			)
		);

		$wp_customize->add_control( 
			'bitstrem_topheader_ctitle', 
			array(
				'label'    =>  __( 'Contact Title', WL_COMPANION_DOMAIN ),
				'type'     => 'text',
				'section'  => 'topheader_section',
				'settings' => 'bitstrem_topheader_ctitle'
			) 
		);

		$wp_customize->add_setting(
		'bitstrem_topheader_cnumber',
			array(
			'default'           => '',
			'type'              => 'theme_mod',
			'sanitize_callback' => 'bitstrem_sanitize_text',
			'capability'        => 'edit_theme_options'
			)
		);

		$wp_customize->add_control( 
			'bitstrem_topheader_cnumber',
			 array(
				'label'    => __( 'Contact Number', WL_COMPANION_DOMAIN ),
				'type'     => 'text',
				'section'  => 'topheader_section',
				'settings' => 'bitstrem_topheader_cnumber'
			) 
		);

		$wp_customize->add_setting(
		'bitstrem_topheader_emailtitle',
			array(
			'default'           => 'Email Us',
			'type'              => 'theme_mod',
			'sanitize_callback' => 'bitstrem_sanitize_text',
			'capability'        => 'edit_theme_options'
			)
		);

		$wp_customize->add_control( 
			'bitstrem_topheader_emailtitle', 
			array(
				'label'    =>  __( 'Email Title', WL_COMPANION_DOMAIN ),
				'type'     => 'text',
				'section'  => 'topheader_section',
				'settings' => 'bitstrem_topheader_emailtitle'
			) 
		);

		$wp_customize->add_setting(
		'bitstrem_topheader_email',
			array(
			'default'           => '',
			'type'              => 'theme_mod',
			'sanitize_callback' => 'bitstrem_sanitize_text',
			'capability'        => 'edit_theme_options'
			)
		);

		$wp_customize->add_control( 
			'bitstrem_topheader_email', 
			array(
				'label'    =>  __( 'Email ID', WL_COMPANION_DOMAIN ),
				'type'     => 'text',
				'section'  => 'topheader_section',
				'settings' => 'bitstrem_topheader_email'
			) 
		);
	}
}

?>