<?php

defined( 'ABSPATH' ) or die();

/**
 *  General options
 */
class wl_footer_customizer {
	
	public static function wl_bitstrem_footer_customizer( $wp_customize ) {
		
		/* Footer Options */
		$wp_customize->add_section(
			'footer_section',array(
				'title'=>__("Footer Options","bitstrem"),
				'panel'=>'bitstrem_theme_option',
				'capability'=>'edit_theme_options',
			    'priority' => 50,
			)
		);

		$wp_customize->add_setting(
			'bitstrem_footer_customization',
			array(
				'default'=>' © Copyright 2019. All Rights Reserved',
				'type'=>'theme_mod',
				'sanitize_callback'=>'bitstrem_sanitize_text',
				'capability'=>'edit_theme_options'
			)
		);

		$wp_customize->selective_refresh->add_partial(
		    'bitstrem_footer_customization', array(
				'selector' => '.copy_bitstrem',
			) 
		);

		$wp_customize->add_control( 'bitstrem_footer_customizationn', array(
			'label'        => __( 'Footer Customization Text', 'bitstrem' ),
			'type'=>'text',
			'section'    => 'footer_section',
			'settings'   => 'bitstrem_footer_customization'
		) );

		$wp_customize->add_setting(
		'bitstrem_develop_by',
			array(
			'default'=>'',
			'type'=>'theme_mod',
			'sanitize_callback'=>'bitstrem_sanitize_text',
			'capability'=>'edit_theme_options'
			)
		);

		$wp_customize->add_control( 
			'bitstrem_develop_byy', 
			array(
				'label'        => __( 'Footer developed by Text', 'bitstrem' ),
				'type'=>'text',
				'section'    => 'footer_section',
				'settings'   => 'bitstrem_develop_by'
			) 
		);

		$wp_customize->add_setting(
		'bitstrem_deve_link',
			array(
			'default'=>'',
			'type'=>'theme_mod',
			'capability'=>'edit_theme_options',
			'sanitize_callback'=>'esc_url_raw'
			)
		);

		$wp_customize->add_control( 
			'bitstrem_deve_linkk', 
			array(
				'label'        => __( 'Footer developed by link', 'bitstrem' ),
				'type'=>'url',
				'section'    => 'footer_section',
				'settings'   => 'bitstrem_deve_link'
			) 
		);
	}
}

?>