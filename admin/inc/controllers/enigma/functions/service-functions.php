<?php 

defined( 'ABSPATH' ) or die();
require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'admin/inc/helpers/wl-companion-helper.php' );

if ( class_exists( 'WP_Customize_Control' ) ) :
class enigma_Customizer_service_fields extends WP_Customize_Control {

	public function render_content() {
		?>
		<div class="wl_agm service_block">
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php endif; ?>
			</label>
			
			<form id="wl-ext-form-service" method="post">
				<div id="input_fields_wrap-service">
					<?php 
					if ( ! empty ( get_theme_mod( 'enigma_service_data') ) )  {
						$name_arr = unserialize(get_theme_mod( 'enigma_service_data'));
						foreach ( $name_arr as $key => $value ) {
						?>
						<div class="wl-dynamic-fields">
							<div class="form-group">
								<label for="title" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Service Title', WL_COMPANION_DOMAIN ); ?></label>
								<input type="text" class="form-control" id="service_title-<?php echo $key; ?>" name="service_title-<?php echo $key; ?>" placeholder="Enter title" value="<?php if ( ! empty ( $value['service_name'] ) ) { echo $value['service_name']; } ?>">
							</div>
							<div class="form-group">
								<label for="service_icon" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Service Icon', WL_COMPANION_DOMAIN ); ?></label>
								<input data-placement="bottomRight" id="service_icon-<?php echo $key; ?>" name="service_icon-<?php echo $key; ?>" class="form-control icp icp-auto-<?php echo $key; ?> service_icon" value="<?php if ( ! empty ( $value['service_icon'] ) ) { echo $value['service_icon']; } ?>" type="text"/>
								<span class="input-group-addon">
									<?php if ( ! empty ( $value['service_icon'] ) ) { ?>
										<i class="<?php echo $value['service_icon']; ?>"></i>
									<?php } ?>
								</span>
								<script type="text/javascript">
									jQuery(document).ready(function ($) {
										jQuery('#service_icon-<?php echo $key; ?>').iconpicker({
												inline: true,
											});
									});
								</script>
							</div>
							<div class="form-group">
								<label for="link" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Service Link', WL_COMPANION_DOMAIN ); ?></label>
								<input type="text" class="form-control" id="service_link-<?php echo $key; ?>" name="service_link-<?php echo $key; ?>" placeholder="Enter Link" value="<?php if ( ! empty ( $value['service_link'] ) ) { echo $value['service_link']; } ?>">
							</div>

							<div class="form-group">
								<label for="service_desc-<?php echo $key; ?>" class="col-form-label wl-txt-label"><?php esc_html_e ( 'Service Description', WL_COMPANION_DOMAIN ); ?></label>
								<textarea class="form-control" rows="5" id="service_desc-<?php echo $key; ?>" name="service_desc-<?php echo $key; ?>" placeholder="Description"><?php if ( ! empty ( $value['service_desc'] ) ) { echo $value['service_desc']; } ?></textarea>
							</div>
							<a href="#" class="btn btn-danger btn-sm remove_field"><?php esc_html_e ( 'Remove', WL_COMPANION_DOMAIN ); ?></a>
						</div>
						<?php
						}
					}
					?>
				</div>
				<button class='btn btn-success add_field_button' id="add_field_button-service"><?php esc_html_e ( 'Add Services', WL_COMPANION_DOMAIN ); ?></button></br>
				<button type="button" class="btn btn-success add_field_button wl-companion-submit-btn" id="wl-ext-submit-service"><?php esc_html_e ( 'Save', WL_COMPANION_DOMAIN ); ?></button>
			</form>
		</div>
		<?php
	}

}
endif;

?>