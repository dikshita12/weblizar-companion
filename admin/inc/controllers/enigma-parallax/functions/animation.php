<?php 

defined( 'ABSPATH' ) or die();

if ( class_exists( 'WP_Customize_Control' ) && ! class_exists( 'enigma_animation' ) ) :
	class enigma_animation extends WP_Customize_Control {

		/**
		 * Render the content on the theme customizer page
		 */
		public function render_content() { ?>
            <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<?php
			$animate_slider   = get_theme_mod( 'animate_type_title', 'fadeIn' );
			$animation        = array(
				'fadeIn',
				'fadeInUp',
				'fadeInDown',
				'fadeInLeft',
				'fadeInRight',
				'bounceIn',
				'bounceInUp',
				'bounceInDown',
				'bounceInLeft',
				'bounceInRight',
				'rotateIn',
				'rotateInUpLeft',
				'rotateInDownLeft',
				'rotateInUpRight',
				'rotateInDownRight',
			); ?>

            <select name="animate_slider" class="webriti_inpute" <?php $this->link(); ?>>
				<?php foreach ( $animation as $animate ) { ?>
                    <option value="<?php echo esc_attr( $animate ); ?>" <?php echo selected( $animate_slider, $animate ); ?>><?php echo esc_attr( $animate ); ?></option>
				<?php } ?>
            </select>
			<?php
		}
	}
endif;