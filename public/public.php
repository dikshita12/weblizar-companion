<?php 

defined( 'ABSPATH' ) or die();
require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'admin/inc/helpers/wl-companion-helper.php' );

$theme_name = wl_companion_helper::wl_get_theme_name();

if ( $theme_name == 'Nineteen' ) {

	/* Custom scripts*/
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/nineteen/wl-scripts.php' );
	add_action( 'wp_footer', array( 'wl_companion_scripts', 'wl_companion_scripts_frontend' ) );

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/nineteen/slider-section.php' );
	add_action( 'wl_companion_slider', array( 'wl_companion_sliders', 'wl_companion_sliders_html' ) );

	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/nineteen/service-section.php' );
	add_action( 'wl_companion_service', array( 'wl_companion_services', 'wl_companion_services_html' ) );

	/* Portfolio Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/nineteen/portfolio-section.php' );
	add_action( 'wl_companion_portfolio', array( 'wl_companion_portfolios', 'wl_companion_portfolios_html' ) );

	/* Client Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/nineteen/client-section.php' );
	add_action( 'wl_companion_client', array( 'wl_companion_clients', 'wl_companion_clients_html' ) );

	/* Team Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/nineteen/team-section.php' );
	add_action( 'wl_companion_team', array( 'wl_companion_teams', 'wl_companion_teams_html' ) );

} elseif ( $theme_name == 'Travelogged' ) {

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/travelogged/slider-section.php' );
	add_action( 'wl_companion_slider_travel', array( 'wl_companion_slider_travel', 'wl_companion_slider_travel_html' ) );

	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/travelogged/service-section.php' );
	add_action( 'wl_companion_services_travel', array( 'wl_companion_services_travel', 'wl_companion_services_travel_html' ) );

	/* Destination Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/travelogged/destination-section.php' );
	add_action( 'wl_companion_destination_travel', array( 'wl_companion_destination_travel', 'wl_companion_destination_travel_html' ) );

	/* Team Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/travelogged/team-section.php' );
	add_action( 'wl_companion_team_travel', array( 'wl_companion_team_travel', 'wl_companion_team_travel_html' ) );

	/* Subscribe Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/travelogged/subscribe-section.php' );
	add_action( 'wl_companion_subscribe_travel', array( 'wl_companion_subscribe_travel', 'wl_companion_subscribe_travel_html' ) );

	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/travelogged/front-end-scripts.php' );
	add_action( 'wp_enqueue_scripts', array( 'wlcm_frontend_scripts', 'frontend_enqueue_assets' ) );

	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/travelogged/subscribe-ajax-action.php' );
	add_action( 'wp_ajax_nopriv_wlc_subscribe_form', array( 'SubscribeFormAjax', 'subscribe_form_action' ) );
	add_action( 'wp_ajax_wlc_subscribe_form', array( 'SubscribeFormAjax', 'subscribe_form_action' ) );

} elseif ( $theme_name == 'Bitstream' ) {

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/bitstream/slider-section.php' );
	add_action( 'wl_companion_slider_bitstream', array( 'wl_companion_sliders_bitstream', 'wl_companion_sliders_bitstream_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/bitstream/service-section.php' );
	add_action( 'wl_companion_service_bitstream', array( 'wl_companion_services_bitstream', 'wl_companion_services_bitstream_html' ) );

	/* Portfolio Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/bitstream/portfolio-section.php' );
	add_action( 'wl_companion_portfolio_bitstream', array( 'wl_companion_portfolios_bitstream', 'wl_companion_portfolios_bitstream_html' ) );
}
elseif ( $theme_name == 'Enigma'|| $theme_name == 'Greenigma' || $theme_name == 'cista' || $theme_name == 'Oculis'|| $theme_name == 'Presto'|| $theme_name == 'Inferno' ) {

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/enigma/slider-section.php' );
	add_action( 'wl_companion_slider_enigma', array( 'wl_companion_sliders_enigma', 'wl_companion_sliders_enigma_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/enigma/service-section.php' );
	add_action( 'wl_companion_service_enigma', array( 'wl_companion_services_enigma', 'wl_companion_services_enigma_html' ) );

	/* Portfolio Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/enigma/portfolio-section.php' );
	add_action( 'wl_companion_portfolio_enigma', array( 'wl_companion_portfolios_enigma', 'wl_companion_portfolios_enigma_html' ) );
}
elseif ( $theme_name == 'enigma-parallax' ) {

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/enigma-parallax/slider-section.php' );
	add_action( 'wl_companion_slider_enigma_parallax', array( 'wl_companion_sliders_enigma_parallax', 'wl_companion_sliders_enigma_parallax_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/enigma-parallax/service-section.php' );
	add_action( 'wl_companion_service_enigma_parallax', array( 'wl_companion_services_enigma_parallax', 'wl_companion_services_enigma_parallax_html' ) );

	/* Portfolio Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/enigma-parallax/portfolio-section.php' );
	add_action( 'wl_companion_portfolio_enigma_parallax', array( 'wl_companion_portfolios_enigma_parallax', 'wl_companion_portfolios_enigma_parallax_html' ) );

	/* Team Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/enigma-parallax/team-section.php' );
	add_action( 'wl_companion_team', array( 'wl_companion_teams', 'wl_companion_teams_html' ) );
}
elseif ( $theme_name == 'Weblizar' ) {

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/weblizar/slider-section.php' );
	add_action( 'wl_companion_slider_wl', array( 'wl_companion_sliders_wl', 'wl_companion_sliders_wl_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/weblizar/service-section.php' );
	add_action( 'wl_companion_service_wl', array( 'wl_companion_services_wl', 'wl_companion_services_wl_html' ) );
}

elseif ( $theme_name == 'Guardian' || $theme_name == 'teckzy' ) {

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/guardian/slider-section.php' );
	add_action( 'wl_companion_slider_guardian', array( 'wl_companion_sliders_guardian', 'wl_companion_sliders_guardian_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/guardian/service-section.php' );
	add_action( 'wl_companion_service_guardian', array( 'wl_companion_services_guardian', 'wl_companion_services_guardian_html' ) );
}

elseif ( $theme_name == 'Creative'|| $theme_name == 'inventive' ) {

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/creative/slider-section.php' );
	add_action( 'wl_companion_slider_creative', array( 'wl_companion_sliders_creative', 'wl_companion_sliders_creative_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/creative/service-section.php' );
	add_action( 'wl_companion_service_creative', array( 'wl_companion_services_creative', 'wl_companion_services_creative_html' ) );

	/* Portfolio Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/creative/portfolio-section.php' );
	add_action( 'wl_companion_portfolio_creative', array( 'wl_companion_portfolios_creative', 'wl_companion_portfolios_creative_html' ) );
}

elseif ( $theme_name == 'Explora' ) {

	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/explora/slider-section.php' );
	add_action( 'wl_companion_slider_explora', array( 'wl_companion_sliders_explora', 'wl_companion_sliders_explora_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/explora/service-section.php' );
	add_action( 'wl_companion_service_explora', array( 'wl_companion_services_explora', 'wl_companion_services_explora_html' ) );

	/* Portfolio Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/explora/portfolio-section.php' );
	add_action( 'wl_companion_portfolio_explora', array( 'wl_companion_portfolios_explora', 'wl_companion_portfolios_explora_html' ) );
}
elseif ( $theme_name == 'scoreline' ) {
	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/scoreline/slider-section.php' );
	add_action( 'wl_companion_slider_scoreline', array( 'wl_companion_sliders_scoreline', 'wl_companion_sliders_scoreline_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/scoreline/service-section.php' );
	add_action( 'wl_companion_service_scoreline', array( 'wl_companion_services_scoreline', 'wl_companion_services_scoreline_html' ) );
}
elseif ( $theme_name == 'Green-Lantern' ) {
	/* Slider Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/green-lantern/slider-section.php' );
	add_action( 'wl_companion_slider_green_lantern', array( 'wl_companion_sliders_green_lantern', 'wl_companion_sliders_green_lantern_html' ) );
	
	/* Service Html */
	require_once( WL_COMPANION_PLUGIN_DIR_PATH . 'public/inc/green-lantern/service-section.php' );
	add_action( 'wl_companion_service_green_lantern', array( 'wl_companion_services_green_lantern', 'wl_companion_services_green_lantern_html' ) );
}
?>