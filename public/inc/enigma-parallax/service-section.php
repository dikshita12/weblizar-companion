<?php

defined( 'ABSPATH' ) or die();

class wl_companion_services_enigma_parallax
{
    
    public static function wl_companion_services_enigma_parallax_html() {
    ?>
        <!-- service section -->
        <div class="clearfix"></div>
        <div  id="service" class="service__section"></div>
        <div class="enigma_service">
            <?php if ( ! empty ( get_theme_mod( 'enigma_service_title' ) ) ) { ?>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="enigma_heading_title">
                                <h3><?php echo get_theme_mod( 'enigma_service_title' ,'Our Services' ); ?></h3>     
                            </div>
                        </div>
                    </div>
                </div>  
            <?php } ?>
            <?php if ( ! empty ( get_theme_mod('enigma_service_data' ) ) ) { ?>
                <div class="container">
                    <div class="row isotope" id="isotope-service-container">        
                        <?php  
                        $name_arr = unserialize(get_theme_mod( 'enigma_service_data'));
                        foreach ( $name_arr as $key => $value ) {
                        ?>
                            <div class=" col-md-4 service">
                                <div class="enigma_service_area appear-animation bounceIn appear-animation-visible">
                                    <?php  if ( ! empty ( $value['service_icon'] ) ) { ?>
                                        <a href="<?php echo $value['service_link']; ?>">
                                            <div class="enigma_service_iocn pull-left">
                                                <i class="<?php echo $value['service_icon']; ?>"></i>
                                            </div>
                                        </a>
                                    <?php } ?> 
                                    <div class="enigma_service_detail media-body">
                                        <?php  if ( ! empty ( $value['service_name'] ) ) { ?>
                                            <h3 class="head_<?php echo esc_attr( $i ) ?>">
                                                <a href="<?php echo $value['service_link']; ?>">
                                                    <?php echo $value['service_name']; ?> 
                                                </a>
                                            </h3>
                                        <?php } 
                                        if ( ! empty ( $value['service_desc'] ) ) { ?>
                                            <p>
                                                <?php echo $value['service_desc']; ?>
                                            </p>
                                        <?php } 
                                        if ( ! empty ( $value['service_link'] ) ) { ?>
                                        <a class="ser-link" href="<?php echo $value['service_link']; ?>">
                                            <?php esc_html_e( 'Read More', 'enigma-parallax' ); ?>
                                        </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>  
                    </div>
                </div>
            <?php } ?> 
        </div>
        <div class="clearfix"></div>
        <!-- /Service section -->
        
    <?php 
    }
}
?>