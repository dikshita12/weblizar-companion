<?php

defined( 'ABSPATH' ) or die();

class wl_companion_services_guardian
{
    
    public static function wl_companion_services_guardian_html() {
    ?>

    <div class="container-fluid feature_section1">
        <div class="container">
            <h2><?php echo get_theme_mod( 'guardian_service_title' ,'Our Service' ); ?>
            </h2>
            <!-- <div class="margin_top3"></div> -->
            <?php if ( ! empty ( get_theme_mod('guardian_service_data' ) ) ) { ?>
                <?php  
                $name_arr = unserialize(get_theme_mod( 'guardian_service_data'));
                foreach ( $name_arr as $key => $value ) {
                ?>
                    <div class="col-md-3 col-sm-6 one_fourth animate" data-anim-type="fadeIn" data-anim-delay="100">        
                        <a href="<?php echo $value['service_link']; ?>">
                            <?php  if ( ! empty ( $value['service_icon'] ) ) { ?>
                                <div class="arrow_box guardian_service_1_icons">
                                    <i class="<?php echo $value['service_icon']; ?>"></i>
                                </div>
                            <?php } ?>
                        
                            <?php  if ( ! empty ( $value['service_name'] ) ) { ?>
                                <h5 class="caps guardian_service_1_title">
                                <?php echo $value['service_name']; ?> 
                                </h5>
                            <?php } ?>
                        </a>
                        <?php 
                        if ( ! empty ( $value['service_desc'] ) ) { ?>
                            <p><?php echo $value['service_desc']; ?></p>
                        <?php }  ?>
                    </div>  
                <?php } ?>
            <?php }  ?>
            
        </div>
    </div><!-- end of service section1 -->
    <div class="clearfix"></div>
        
    <?php 
    }
}
?>