<?php

defined( 'ABSPATH' ) or die();


class wl_companion_services_enigma
{
    
    public static function wl_companion_services_enigma_html() {
        $theme_name = wl_companion_helper::wl_get_theme_name();
    ?>
        <!-- service section -->
        <div class="enigma_service <?php if ( $theme_name == 'Oculis' ) { ?>service2<?php } ?>">
            <?php if ( ! empty ( get_theme_mod( 'enigma_service_title' ) ) ) { ?>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="enigma_heading_title">
                                <h3><?php echo get_theme_mod( 'enigma_service_title' ,'Our Service' ); ?></h3>     
                            </div>
                        </div>
                    </div>
                </div>  
            <?php } ?>
            <?php if ( ! empty ( get_theme_mod('enigma_service_data' ) ) ) { ?>
            <div class="container">
                <div class="row isotope" id="isotope-service-container">        
                    <?php  
                    $name_arr = unserialize(get_theme_mod( 'enigma_service_data'));
                    foreach ( $name_arr as $key => $value ) {
                    ?>
                    <div class=" col-md-4 service">
                        <div class="enigma_service_area appear-animation bounceIn appear-animation-visible">
                            <?php 
                          
                            if ( ! empty ( $value['service_icon'] ) ) { ?>
                                <a href="<?php echo $value['service_link']; ?>">
                                    <div class="enigma_service_iocn pull-left">
                                        <i class="<?php echo $value['service_icon']; ?>"></i>
                                    </div>
                                </a>
                            <?php } ?> 
                            <div class="enigma_service_detail media-body">
                                <?php 
                                if ( ! empty ( $value['service_name'] ) ) { ?>
                                    <h3 class="head">
                                        <a href="<?php echo $value['service_link']; ?>">
                                           <?php echo $value['service_name']; ?> 
                                        </a>
                                    </h3>
                                <?php } 
                               
                                if ( ! empty ( $value['service_desc'] ) ) { ?>
                                    <p><?php echo $value['service_desc']; ?></p>
                                <?php } 
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        </div>   
        <!-- /Service section -->
    <?php 
    }
}
?>