<?php

defined( 'ABSPATH' ) or die();

class wl_companion_sliders_enigma
{
    
    public static function wl_companion_sliders_enigma_html() {
    ?>
        <?php
        $slider_anim = get_theme_mod('slider_anim');
        if ($slider_anim == 'fadeIn') {
            $slider_class = 'fadein';
        } else {
            $slider_class = 'slide';
        }
        ?>
        <?php if ( ! empty ( get_theme_mod('enigma_slider_data' ) ) ) { ?>

        <div id="myCarousel" class="carousel  <?php echo esc_attr($slider_class); ?>" data-ride="carousel">
        <div class="carousel-inner">
            <?php 
           
            $j = 1;
             $name_arr = unserialize(get_theme_mod( 'enigma_slider_data'));
            foreach ( $name_arr as $key => $value ) {
                
            ?>
                <div class="carousel-item <?php if ($j == 1) echo "active"; ?>">
                    <img src="<?php echo $value['slider_image']; ?>"
                         class="img-responsive"
                         alt="<?php if ( ! empty ( $value['slider_name'] ) ) { echo $value['slider_name']; } ?>">
                    <div class="container">
                        <div class="carousel-caption">
                            <div class="carousel-text">
                                <?php 
                                $animate_type_title = get_theme_mod('animate_type_title');
                                if ( ! empty ( $value['slider_name'] ) ) { ?>
                                    <h1 class="animated <?php if (!empty ($animate_type_title)) {
                                        echo esc_attr(get_theme_mod('animate_type_title'));
                                    } else echo esc_attr('bounceInRight'); ?>"><?php echo $value['slider_name']; ?>
                                    </h1>
                                <?php }
                                $animate_type_desc = get_theme_mod('animate_type_desc');
                                if ( ! empty ( $value['slider_desc'] ) ) { ?>
                                    <ul class="list-unstyled carousel-list">
                                        <li class="animated <?php if (!empty ($animate_type_desc)) {
                                            echo esc_attr(get_theme_mod('animate_type_desc'));
                                        } else esc_attr('bounceInLeft'); ?>">
                                        <?php echo $value['slider_desc']; ?>
                                        </li>
                                    </ul>
                                <?php }
                                if ( ! empty ( $value['slider_link'] ) ) { ?>
                                    <a class="enigma_blog_read_btn animated bounceInUp"
                                       href="<?php echo $value['slider_link'];  ?>"
                                       role="button">
                                       <?php if ( ! empty ( $value['slider_text'] ) ) {  echo $value['slider_text']; } ?> </a>
                                <?php } ?>
                            </div>
                            
                        </div>
                    </div>
                </div>
            <?php $j++; 
        
             } ?>
        </div>
        <ol class="carousel-indicators">
            <?php for ($i = 0;
            $i<$j-1;
            $i++) { ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo esc_attr($i); ?>" <?php if ($i==0) {
            echo 'class="active"';
            } ?> ></li>
            <?php } ?>
        </ol>
        <a class="carousel-control-prev" href="#myCarousel" data-slide="prev"><span
                    class="carousel-control-prev-icon"></span></a>
        <a class="carousel-control-next" href="#myCarousel" data-slide="next"><span
                    class="carousel-control-next-icon"></span></a>
        <div class="enigma_slider_shadow"></div>
    </div>
    <!-- /.carousel -->
<?php } } } ?>