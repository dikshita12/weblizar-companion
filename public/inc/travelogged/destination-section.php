<?php

defined( 'ABSPATH' ) or die();

class wl_companion_destination_travel {
    
    public static function wl_companion_destination_travel_html() {
    ?>
    <!--Booking Section -->
    <section class="property_booking space">
        <div class="container">
            <!--section-heading-->
            <div class="section-heading  text-center">
            <?php if ( ! empty ( get_theme_mod( 'travelogged_destination_title' ) ) ) { ?>
                <h2 class="section-title"><span> <?php echo get_theme_mod( 'travelogged_destination_title' ,'Proper Destination' ); ?></span></h2>
            <?php } if ( ! empty ( get_theme_mod( 'travelogged_destination_desc' ) ) ) { ?>
                <p class="text-white"><?php echo get_theme_mod( 'travelogged_destination_desc' ); ?></p>
            <?php } ?>
            </div>
            <!--owl carousel slider-->
            <div class="owl-carousel owl-carousel_3 owl-theme owl-nav_mainclr owl_btn2 wow animated fadeInDown" data-wow-duration="1s" data-wow-offset="150">
            <?php  if ( ! empty ( get_theme_mod('travelogged_destination_data' ) ) ) {
                    $name_arr = unserialize(get_theme_mod( 'travelogged_destination_data'));
                    foreach ( $name_arr as $key => $value ) {
            ?>
                <!--item slider-->
                <div class="item">
                    <figure>
                        <div class="property_img">
                        <?php if ( ! empty ( $value['desti_image'] ) ) { ?>
                            <img src="<?php echo esc_attr( esc_url( $value['desti_image'] ) ); ?>" class="img-fluid" alt="<?php echo esc_attr( $value['desti_name'] ); ?>"/>
                        <?php } if ( ! empty ( $value['desti_duration'] ) ) { ?>
                            <h6 class="spof_days"><i class="far fa-clock icon"></i><?php echo esc_html( $value['desti_duration'] ); ?> </h6>
                        <?php } ?>
                        </div>
                        <figcaption class="property_cont">
                        <?php if ( ! empty ( $value['desti_name'] ) ) { ?>
                            <h4><span> <?php echo esc_html( $value['desti_name'] ); ?> </span></h4>
                        <?php } if ( ! empty ( $value['desti_rating'] ) ) { ?>
                            <span class="spof_ratting">
                            <?php if ( $value['desti_rating'] == 1 ) { ?>
                            <i class="fas fa-star icon"></i>
                            <?php } elseif ($value['desti_rating'] == 2 ) { ?>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <?php } elseif ($value['desti_rating'] == 3 ) { ?>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <?php } elseif ($value['desti_rating'] == 4 ) { ?>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <?php } elseif ($value['desti_rating'] == 5 ) { ?>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <i class="fas fa-star icon"></i>
                            <?php } ?>
                            </span>
                        <?php } if ( ! empty ( $value['desti_desc'] ) ) { ?>
                            <p><?php echo esc_html( $value['desti_desc'] ); ?></p>
                            <?php } ?>
                            <?php if ( ! empty ( $value['desti_link'] ) ) { ?>
                            <a href="<?php echo esc_url( $value['desti_link'] ); ?>" class="btn mb-2"> <?php echo esc_html( $value['desti_text'] ); ?></a>
                            <?php } ?>
                        </figcaption>
                    </figure>
                </div>
                <!--item slider-->
            <?php } } ?>
            </div>
        </div>
    </section>
    <?php
    }
}
?>