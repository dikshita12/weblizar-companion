<?php

defined( 'ABSPATH' ) or die();

class wl_companion_subscribe_travel {
    
    public static function wl_companion_subscribe_travel_html() {
    ?>
        <!--Subscribe Section-->
        <section class="subscribe space">
            <div class="container">
                <div class="subscribe-cont">
                    <h3> <?php echo esc_html( get_theme_mod( 'travelogged_subscribe_title','Subscribe') ); ?> </h3>
                    <h2> <?php echo esc_html( get_theme_mod( 'travelogged_subscribe_title1','FOR NEWSLETTER') ); ?></h2>
                    <p> <?php echo esc_html( get_theme_mod( 'travelogged_subscribe_desc' ) ); ?></p>
                </div>
                <div class="input-group row mt-4">
                    <input type="email" class="form-control" id="subscribe_mail" placeholder="Enter your email">
                    <button type="button" id="subscribe_home_btn" class="btn mb-2">
                        <?php echo esc_html( get_theme_mod( 'travelogged_subscribe_btntext','Subscribe') ); ?>
                    </button>
                </div>
            </div>
        </section>
    <?php 
    }
}

?>